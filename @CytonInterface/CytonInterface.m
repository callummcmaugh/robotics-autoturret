classdef CytonInterface < handle
   % Define property listeners
   properties
       robot_s;
       robot_r;
   end
   methods
      function obj = CytonInterface(robot_sim, robot_real)
         if nargin > 0
            addlistener(robot_sim,'q','PostSet',@obj.handlePropEvents);
%             addlistener(robot_sim,'T','PostSet',@CytonInterface.handlePropEvents);
            obj.robot_s = robot_sim;
            obj.robot_r = robot_real;
         end
      end
      function handlePropEvents(self, src, event)
%           name = src.Name
%           evnt
%          self
         switch src.Name
            case 'q'
%                 src.Name
%                 sprintf('q change');
                self.robot_s.q
                self.robot_r.MoveAllJoints( self.robot_s.q)
            case 'T'
%                 sprintf('T change');
         end
      end
   end
end