% Basic code to start off reading an image from ROS.

classdef CytonROS < handle
    properties
        % Cyton State
        motor_state = 0;
        gripper_state = 0;
        % Properties for energising the cyton
        cute_enable_robot_client;
        cute_enable_robot_msg;
        
        % Gripper Control
        cute_enable_gripper_client;
        cute_enable_gripper_msg;
        
        % Move All Joints Client
        cute_move_client;
        cute_move_msg;
        
        % Move Single Joint Client
        cute_single_joint_client;
        cute_single_joint_msg;
    end
    methods
        function self = CytonROS()
            self.cute_enable_robot_client = rossvcclient('enableCyton');
            self.cute_enable_robot_msg = rosmessage(self.cute_enable_robot_client);
            
            self.cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable');
            self.cute_enable_gripper_msg = rosmessage(self.cute_enable_gripper_client);
            
            self.cute_move_client = rossvcclient('/GoToPosition');
            self.cute_move_msg = rosmessage(self.cute_move_client);
            
            self.cute_single_joint_client = rossvcclient('/MoveSingleJoint');
            self.cute_single_joint_msg = rosmessage(self.cute_single_joint_client);
            
            self.motor_state = 0;
            self.gripper_state = 0;
        end
        
        function Enable(self)
            self.MotorOn();
%             self.GripperOn();
            self.motor_state = 1;
%             self.gripper_state = 1;
        end
        function Disable(self)
            self.MotorOff();
%             self.GripperOff();
            self.motor_state = 0;
%             self.gripper_state = 0;
        end
        
        function MotorOn(self)
            self.cute_enable_robot_msg.TorqueEnable = true;
            self.cute_enable_robot_client.call(self.cute_enable_robot_msg);
        end
        function MotorOff(self)
            self.cute_enable_robot_msg.TorqueEnable = false;
            self.cute_enable_robot_client.call(self.cute_enable_robot_msg);
        end
        function GripperOn(self)
            self.cute_enable_gripper_msg.TorqueEnable = 1;
            self.cute_enable_gripper_client.call(self.cute_enable_gripper_msg);
        end
        function GripperOff(self)
            self.cute_enable_gripper_msg.TorqueEnable = 0;
            self.cute_enable_gripper_client.call(self.cute_enable_gripper_msg);
        end
        function MoveAllJoints(self, j)
            if(self.motor_state == 0)
                return
            end
            self.cute_move_msg.JointStates = j;
            self.cute_move_client.call(self.cute_move_msg, 'Timeout', 10); 
        end
        
        function MoveSingleJoint(self, joint, q)
            if(self.motor_state == 0)
                return
            end
            self.cute_single_joint_msg.JointNumber = joint;% Joints 0-6
            self.cute_single_joint_msg.Position = q;% (Rads)
            self.cute_single_joint_client.call(self.cute_single_joint_msg, 'Timeout', 10);
        end
    end
    
end