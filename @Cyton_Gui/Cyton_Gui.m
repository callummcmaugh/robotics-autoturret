function varargout = Cyton_Gui(varargin)
% CYTON_GUI MATLAB code for Cyton_Gui.fig
%      CYTON_GUI, by itself, creates a new CYTON_GUI or raises the existing
%      singleton*.
%
%      H = CYTON_GUI returns the handle to a new CYTON_GUI or the handle to
%      the existing singleton*.
%
%      CYTON_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CYTON_GUI.M with the given input arguments.
%
%      CYTON_GUI('Property','Value',...) creates a new CYTON_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Cyton_Gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Cyton_Gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Cyton_Gui

% Last Modified by GUIDE v2.5 03-Jun-2019 01:27:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Cyton_Gui_OpeningFcn, ...
                   'gui_OutputFcn',  @Cyton_Gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end


%% --- Executes just before Cyton_Gui is made visible.
function Cyton_Gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Cyton_Gui (see VARARGIN)
varargin{1};
% % robot = varargin(1)
% 
% return

robot = varargin{1};
handles.robot = robot;

robot.PlotRobot3d();
hold on;
handles.camera = RGBCam;
handles.table = Table;
handles.barrier1 = Barrier;
handles.barrier2 = Barrier;
handles.barrier3 = Barrier;
handles.barrier4 = Barrier;

% handles.listener = addlistener(handles.robot,'q','PostSet',@updateJoints);

barrier1Position = transl(0.011,-0.27,-0.035);
barrier2Position = transl(-0.25,0,-0.035)*trotz(pi/2);
barrier3Position = transl(0.23,0,-0.035)*trotz(pi/2);
barrier4Position = transl(0.011,0.23,-0.035);

handles.barrier1.UpdatePos(barrier1Position);
handles.barrier2.UpdatePos(barrier2Position);
handles.barrier3.UpdatePos(barrier3Position);
handles.barrier4.UpdatePos(barrier4Position);
handles.camera.UpdatePos(transl(0,0.1,-0.068)*trotz(pi/2));
handles.table.UpdatePos(transl(0,0,-0.43));
%% setting up cartesian movement transforms
handles.positiveX = transl(0.01,0,0);
handles.negativeX = transl(-0.01,0,0);
handles.positiveY = transl(0,0.01,0);
handles.negativeY = transl(0,-0.01,0);
handles.positiveZ = transl(0,0,0.01);
handles.negativeZ = transl(0,0,-0.01);
handles.newPositiveX = transl(0,0,0);
%% deafaulting text boxe values to 0 for displaying joint start angles
set(handles.text1,'String',num2str(0));
set(handles.text2,'String',num2str(0));
set(handles.text3,'String',num2str(0));
set(handles.text4,'String',num2str(0));
set(handles.text5,'String',num2str(0));
set(handles.text6,'String',num2str(0));
set(handles.text7,'String',num2str(0));
%%
% handles.listener = addlistener(handles.robot, 'q', 'PostSet', @updateJoints)
% Choose default command line output for Cyton_Gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Cyton_Gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end


%% --- Outputs from this function are returned to the command line.
function varargout = Cyton_Gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end


%% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider1, 'Value');
myRobot.jointOne(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider1,'Value');
set(handles.text1,'String',num2str(sliderValue*180/pi));
else
end

guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider2, 'Value');
myRobot.jointTwo(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider2,'Value');
set(handles.text2,'String',num2str(sliderValue*180/pi));
else
end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider3, 'Value');
myRobot.jointThree(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider3,'Value');
set(handles.text3,'String',num2str(sliderValue*180/pi));
else
end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider4, 'Value');
myRobot.jointFour(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider4,'Value');
set(handles.text4,'String',num2str(sliderValue*180/pi));
else
end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
 if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider5, 'Value');
myRobot.jointFive(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider5,'Value');
set(handles.text5,'String',num2str(sliderValue*180/pi));
 else
 end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider6, 'Value');
myRobot.jointSix(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider6,'Value');
set(handles.text6,'String',num2str(sliderValue*180/pi));
    else
    end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on slider movement.
function slider7_Callback(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
myRobot = handles.robot;
if myRobot.GetEStopStatus == 0
sliderVal = get(handles.slider7, 'Value');
myRobot.jointSeven(sliderVal);
myRobot.playRobot3d(myRobot.getJointState());
sliderValue = get(handles.slider7,'Value');
set(handles.text7,'String',num2str(sliderValue*180/pi));
else
end
guidata(hObject, handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
end


% --- Executes during object creation, after setting all properties.
function slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end


%% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.positiveX);
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    else
    end
    
    myRobot.playRobot3d(newPos);
    guidata(hObject, handles);
end

%% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.positiveY);
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    else
    end
    
    myRobot.playRobot3d(newPos);
    guidata(hObject, handles);
end

%% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.positiveZ);
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    else
    end
    
    myRobot.playRobot3d(newPos);
    guidata(hObject, handles);
end

%% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.negativeX);
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    else
    end
    
    myRobot.playRobot3d(newPos);
    guidata(hObject, handles);
end
%% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.negativeY);
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    else
    end
    
    myRobot.playRobot3d(newPos);
   
    
    guidata(hObject, handles);
end

%% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    myRobot = handles.robot;
    if myRobot.GetEStopStatus == 0
    currentPos = myRobot.getCurrentTransforms();
    newPos = myRobot.Ikine(currentPos*handles.negativeZ);
    
    set(handles.slider1, 'Value', newPos(1,1));
    set(handles.slider2, 'Value', newPos(1,2));
    set(handles.slider3, 'Value', newPos(1,3));
    set(handles.slider4, 'Value', newPos(1,4));
    set(handles.slider5, 'Value', newPos(1,5));
    set(handles.slider6, 'Value', newPos(1,6));
    set(handles.slider7, 'Value', newPos(1,7));
    
    set(handles.text1,'String',num2str(newPos(1,1)*180/pi));
    set(handles.text2,'String',num2str(newPos(1,2)*180/pi));
    set(handles.text3,'String',num2str(newPos(1,3)*180/pi));
    set(handles.text4,'String',num2str(newPos(1,4)*180/pi));
    set(handles.text5,'String',num2str(newPos(1,5)*180/pi));
    set(handles.text6,'String',num2str(newPos(1,6)*180/pi));
    set(handles.text7,'String',num2str(newPos(1,7)*180/pi));
    
    myRobot.playRobot3d(newPos);
    else
    end
    guidata(hObject, handles);
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
    myRobot = handles.robot;
    myRobot.EnableEStop();
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    guidata(hObject, handles);
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
    myRobot = handles.robot;
    myRobot.DisableEStop();
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    guidata(hObject, handles);
end



