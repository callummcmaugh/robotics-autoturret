
qStart = [-0.3,pi/2,0,-pi/2,0,pi/2,0];
qPos1 = [0,pi/2,0,-pi/2,0,pi/2,0];
qPos2 = [0.3,pi/2,0,-pi/2,0,pi/2,0];
qPos3 = [0.3,pi/4,0,-pi/4,0,pi/2,0];
qPos4 = [0,pi/4,0,-pi/4,0,pi/2,0];
qFinish = [-0.3,pi/4,0,-pi/4,0,pi/2,0];

% cyton_r.MoveAllJoints(qStart)
% pause(0.1)
% cyton_r.MoveAllJoints(qPos1)
% pause(0.1)
% cyton_r.MoveAllJoints(qPos2)
% pause(0.1)
% cyton_r.MoveAllJoints(qPos3)
% pause(0.1)
% cyton_r.MoveAllJoints(qPos4)
% pause(0.1)
% cyton_r.MoveAllJoints(qFinish)
% pause(0.1)
% cyton_r.MoveAllJoints(qStart)
% pause(0.1)

cyton_s.Plot(qStart)
pause(0.1)
cyton_s.Plot(qPos1)
pause(0.1)
cyton_s.Plot(qPos2)
pause(0.1)
cyton_s.Plot(qPos3)
pause(0.1)
cyton_s.Plot(qPos4)
pause(0.1)
cyton_s.Plot(qFinish)
pause(0.1)
cyton_s.Plot(qStart)
pause(0.1)