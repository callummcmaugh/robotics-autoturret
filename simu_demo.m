% 

cyton_s = Cyton();
cyton_s.PlotRobot3d();
Cyton_Gui(cyton_s);
cyton_r =CytonROS();
cyton_r.Enable();
cyton_i = CytonInterface(cyton_s, cyton_r);

hold on;
Desk = Table;
Cam = RGBCam;
barrier1 = Barrier;
barrier2 = Barrier;
barrier3 = Barrier;
barrier4 = Barrier;

camPositon = transl(0,0.1,-0.068)*trotz(pi/2);
deskPosition = transl(0,0,-0.43);
barrier1Position = transl(0.011,-0.27,-0.035);
barrier2Position = transl(-0.25,0,-0.035)*trotz(pi/2);
barrier3Position = transl(0.23,0,-0.035)*trotz(pi/2);
barrier4Position = transl(0.011,0.23,-0.035);

originalJointState = cyton_s.getJointState();
Desk.UpdatePos(deskPosition);
Cam.UpdatePos(camPositon);
barrier1.UpdatePos(barrier1Position);
barrier2.UpdatePos(barrier2Position);
barrier3.UpdatePos(barrier3Position);
barrier4.UpdatePos(barrier4Position);

firstPoint = transl(0.5,0.5,0.5);
secondPoint = transl(0.2, 0.5, 0.5);
thirdPoint = transl(-0.8, 0.7, 0.2);
fourthPoint = transl(-0.2, 0.2, 1);
fifthPoint = transl(0.3, 0.7, 0.4);
sixthPoint = transl(-0.1, 0.3, 0.6);
seventhPoint = transl(-0.4, 0.2, 0.2);
eigthPoint = transl(0.5, 0.7, 0.8);
ninthPoint = transl(-0.5, 0.7, 0.6);
tenthPoint = transl(0.4,0.4,0.4);

first_JointState = cyton_s.Ikine(firstPoint);
second_JointState = cyton_s.Ikine(secondPoint);
third_JointState = cyton_s.Ikine(thirdPoint);
fourth_JointState = cyton_s.Ikine(fourthPoint);
fifth_JointState = cyton_s.Ikine(fifthPoint);
sixth_JointState = cyton_s.Ikine(sixthPoint);
seventh_JointState = cyton_s.Ikine(seventhPoint);
eigth_JointState = cyton_s.Ikine(eigthPoint);
ninth_JointState = cyton_s.Ikine(ninthPoint);
tenth_JointState = cyton_s.Ikine(tenthPoint);

firstMove = cyton_s.calculateTraj(originalJointState,first_JointState); 
secondMove = cyton_s.calculateTraj(first_JointState,second_JointState); 
thirdMove = cyton_s.calculateTraj(second_JointState,third_JointState);
fourthMove = cyton_s.calculateTraj(third_JointState,fourth_JointState);
fifthMove = cyton_s.calculateTraj(fourth_JointState,fifth_JointState);
sixthMove = cyton_s.calculateTraj(fifth_JointState,sixth_JointState);
seventhMove = cyton_s.calculateTraj(sixth_JointState,seventh_JointState);
eigthMove = cyton_s.calculateTraj(seventh_JointState,eigth_JointState);
ninthMove = cyton_s.calculateTraj(eigth_JointState,ninth_JointState);
tenthMove = cyton_s.calculateTraj(ninth_JointState,tenth_JointState);

pause(2);


playOneRobot(cyton_s,firstMove,firstPoint,camPositon);
playOneRobot(cyton_s,secondMove,secondPoint,camPositon);
playOneRobot(cyton_s,thirdMove,thirdPoint,camPositon);
playOneRobot(cyton_s,fourthMove,fourthPoint,camPositon);
playOneRobot(cyton_s,fifthMove,fifthPoint,camPositon);
playOneRobot(cyton_s,sixthMove,sixthPoint,camPositon);
playOneRobot(cyton_s,seventhMove,seventhPoint,camPositon);
playOneRobot(cyton_s,eigthMove,eigthPoint,camPositon);
playOneRobot(cyton_s,ninthMove,ninthPoint,camPositon);
playOneRobot(cyton_s,tenthMove,tenthPoint,camPositon);

function playOneRobot(robot1,traj1,pose,camPos)

    for i = 1:200
        
        robot1.playRobot3d(traj1(i,1:7));
        trajPoses = robot1.Fkine(traj1(i,1:7));
        startP = trajPoses(1:3,4);
        camstartP = camPos(1:3,4);
        tr = pose;
        endP = tr(1:3,4);
        if robot1.GetEStopStatus == 0
        robotToPoint = plot3([startP(1),endP(1)],[startP(2),endP(2)],[startP(3),endP(3)]);
        camToPoint = plot3([camstartP(1),endP(1)],[camstartP(2),endP(2)],[camstartP(3),endP(3)]);
        point = plot3(endP(1),endP(2),endP(3),'-o');
        pause(0.01);
        delete(robotToPoint);
        delete(camToPoint);
        delete(point);
        end
    end

end