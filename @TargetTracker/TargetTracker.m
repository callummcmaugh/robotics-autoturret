classdef TargetTracker < handle
    properties(SetObservable)
        trans;
        rot;
        t;
        robot_s;
%         robot_r
        tf_sub;
        plot_h;
        cam_pos_x = -0.15;
        cam_pos_y = 0;
        cam_pos_z = 0;
        cam_rot_x = deg2rad(0);
        cam_rot_y = deg2rad(-45);
        cam_rot_z = deg2rad(180);
        camera_t;
        camera_h;
    end
    methods
        function obj = TargetTracker(robot_s)
            obj.CreateTracker()
            obj.robot_s = robot_s;
%             obj.robot_r = robot_r;
            obj.camera_t = transl(obj.cam_pos_x, obj.cam_pos_y, obj.cam_pos_z)...
                *trotx(obj.cam_rot_x)*troty(obj.cam_rot_y)*trotz(obj.cam_rot_z)
            try delete(obj.camera_h); end
            obj.camera_h = trplot(obj.camera_t,'length', 0.1, 'frame', 'CAMERA')
        end
        
        function CreateTracker(obj)
            obj.tf_sub = rossubscriber('/tf', @obj.CallbackTF);
            pause(1);
        end
        
        function PlotTF(obj)
           try delete(obj.plot_h); end
           figure(1)
           ObjectT = inv(obj.camera_t)*obj.t;
           obj.plot_h = trplot(ObjectT, 'length', 0.1, 'frame', 'AR');
           
           q = obj.robot_s.Ikine(ObjectT);
%            q = obj.robot_s.IkineMask(ObjectT);
           obj.robot_s.Plot(q);
        end
        
        function PlotOnRobot(obj)
            try delete(obj.plot_h); end
%             RobotT = obj.robot.getCurrentTransforms();
%             ObjectT = obj.t;
%             RelativeT = inv(RobotT)*ObjectT
%             obj.plot_h = trplot(RelativeT)
%             q = obj.robot.Ikine(RelativeT);
%             obj.robot.Plot(q);

%             CameraLoc = transl(obj.cam_pos_x, obj.cam_pos_y, obj.cam_pos_z)...
%                 *trotx(obj.cam_rot_x)*troty(obj.cam_rot_y)*trotz(obj.cam_rot_z)
%             trplot(CameraLoc,'length', 0.1, 'frame', 'CAMERA')
%             obj.t = eye(4)
            
%             q = obj.robot_r.Ikine(ObjectT);
%             obj.robot.Plot(q);
%             q = obj.robot_s
%             obj.robot_r.MoveAllJoints(q)
        end
        
        function CallbackTF(obj, src, message)
%             message
            if(src.TopicName == "/tf")
                frame = message.Transforms.ChildFrameId;
                if(frame ~= "claw1")
%                     mt = message.Transforms.MessageType
%                     head = message.Transforms.Header
%                     frame = message.Transforms.ChildFrameId
%                     trans = message.Transforms.Transform
                    mtrans = message.Transforms.Transform.Translation;
                    mrot = message.Transforms.Transform.Rotation;
                    obj.trans = [ mtrans.X, mtrans.Y, mtrans.Z]
                    quat = [mrot.X, mrot.Y, mrot.Z, mrot.W];
                    obj.rot = quat2eul(quat);
                    obj.t = transl(obj.trans)*trotx(obj.rot(1))*troty(obj.rot(2))*trotz(obj.rot(3));

                    
                    obj.PlotTF();
%                     obj.PlotOnRobot();
                end
            end
%             src
%             message.Transforms
%             mt = message.Transforms.MessageType
%             head = message.Transforms.Header
%             frame = message.Transforms.ChildFrameId
%             trans = message.Transforms.Transform
%             mtrans = message.Transforms.Transform.Translation;
%             mrot = message.Transforms.Transform.Rotation;
%             obj.trans = [ mtrans.X, mtrans.Y, mtrans.Z]
%             quat = [mrot.X, mrot.Y, mrot.Z, mrot.W];
%             obj.rot = quat2eul(quat);
%             obj.t = transl(obj.trans)*trotx(obj.rot(1))*troty(obj.rot(2))*trotz(obj.rot(3));
            
%             obj.PlotOnRobot();
%             obj.PlotTF();
        end
        
        function T = GetTranslation(obj)
            T = obj.trans; 
        end
        function R = GetRotation(obj)
            R = obj.trans;
        end
        function T = GetTransform(obj)
            T = obj.t;
        end
        function ClearCB(obj)
            obj.tf_sub = [];
        end
    end   
end