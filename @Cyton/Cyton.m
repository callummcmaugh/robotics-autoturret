 % Configuration of class for Cyton Robot.

% DH Parameters and seriallink etc.

classdef Cyton < handle
    properties (SetObservable)
        model;
        name;
        T = eye(4);
        q = [0 0 0 0 0 0 0];    
        offset = [0 0 0 pi/2 0 -pi/2 pi/2];
        d = [62.8 0 (76.827+48.83) 0 0 0 (27)] / 1000;
        a = [ 0 0 0 -66.63 -66.63 0 0] / 1000;
        alpha = [-pi/2 pi/2 -pi/2 pi/2 -pi/2 pi/2 0];
        base_offset = [0 0 0] / 1000;
        qlim = [-2*pi 2*pi];
        scale = 0.5;
        workspace = [-1 1 -1 1 -1 1];  
        e_stop = 0;
    end
    
    %% Class for Cyton robot simulation
    methods
        function self = Cyton(name)
            if  nargin > 0
                self.GetCytonRobot(name);
            else
                self.GetCytonRobot("simu");
            end
        end
        % GetCytonRobot
        % Given a name (optional), create and return a Cyton robot model
        function GetCytonRobot(self, name)
            pause(0.001);
            self.name = 'Cyton-' + name;
            L1 = Link('d',0.0628,'a',0,'alpha',pi/2,'qlim',[deg2rad(-150) deg2rad(150)]);
            L2 = Link('d',0,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)]);
            L3 = Link('d',0.125657,'a',0,'alpha',pi/2,'qlim',[deg2rad(-150) deg2rad(150)]);
            L4 = Link('d',0,'a',0.06663,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)],'offset',pi/2);
            L5 = Link('d',0,'a',0.06663,'alpha',pi/2,'qlim',[deg2rad(-105) deg2rad(105)]);
            L6 = Link('d',0,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)],'offset',-pi/2);
            L7 = Link('d',0.1388,'a',0,'alpha',0,'qlim',[deg2rad(-150) deg2rad(150)]);
%             L1 = Link('revolute', 'd', self.d(1),'a',self.a(1),'alpha',self.alpha(1),'offset', self.offset(1),'qlim',[-pi, pi]);
%             L2 = Link('revolute', 'd', self.d(2),'a',self.a(2),'alpha',self.alpha(2),'offset', self.offset(2),'qlim',[-pi, pi]);%self.qlim);
%             L3 = Link('d',self.d(3),'a',self.a(3),'alpha',self.alpha(3),'offset',self.offset(3),'qlim',self.qlim);
%             L4 = Link('d',self.d(4),'a',self.a(4),'alpha',self.alpha(4),'offset',self.offset(4),'qlim',self.qlim);
%             L5 = Link('d',self.d(5),'a',self.a(5),'alpha',self.alpha(5),'offset',self.offset(5),'qlim',self.qlim);
%             L6 = Link('d',self.d(6),'a',self.a(6),'alpha',self.alpha(6),'offset',self.offset(6),'qlim',self.qlim);
%             L7 = Link('d',self.d(7),'a',self.a(7),'alpha',self.alpha(7),'offset',self.offset(7),'qlim',self.qlim);
            self.model = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name',self.name);
        end
        
        function base = getBase(self)
            
            base = self.model.base;
        end
        
        function links = getLinks(self)
           
            links = self.model.links;
        end
        
        function EnableEStop(self)
           self.e_stop = 1;
        end
        function DisableEStop(self)
           self.e_stop = 0;
        end
        function status = GetEStopStatus(self)
            status = self.e_stop
        end
        function Start(self, q)
          self.model.plot(q, 'scale', self.scale, 'noarrow')
          hold on;
%           self.start_q = q;
          self.q = q;
       end
        function Plot(self, Q)
           self.model.plot(Q, 'scale', self.scale, 'noarrow');
           self.q = Q;
        end 
        function PlotRobot3d(self)
            for linkIndex = 0:self.model.n
                    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['CytonLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex+1} = faceData;
                self.model.points{linkIndex+1} = vertexData;
            end

            % Display robot
            self.model.plot3d(self.q,'noarrow','workspace',self.workspace, 'view', [180 45]);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end  
            self.model.delay = 0;

            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
       
        function Teach(self)
           self.model.teach
        end
        
        function playRobot3d(self, matrix)
            if self.e_stop == 0
            animate(self.model, matrix);
            self.q = matrix;
            T = self.model.fkine(matrix);
            self.T = T;
            else
                
            end
    
        end
        
        function SetBase(self, Transform)
            self.model.base = Transform;
        end
        
        function SetBaseXYZRPY(self, x, y, z, roll, pitch, yaw)
            self.model.base = transl(x, y, z);
            self.model.base = self.model.base*trotx(roll);
            self.model.base = self.model.base*troty(pitch);
            self.model.base = self.model.base*trotz(yaw);
        end
        function T = Fkine(self, q)
            T = self.model.fkine(q);
            self.T = T;
            self.q = q;
        end
        
        function q = Ikine(self, T)
            iKineAlpha = 0.1;
            % mask = [1,1,1,1,1,1,1];
            q = self.model.ikcon(T,  self.q);
            self.q = q;
            self.T = T;
%             q = self.model.ikine(T,self.start_q, mask ,'alpha',iKineAlpha,'ilimit',1000);
        end
        
        function q = IkineMask(self, T)
            iKineAlpha = 0.1;
            mask = [1,1,1,0,0,0];
            q = self.model.ikcon(T,  self.q, 'Mask', mask);
            
%             q = self.model.ikine(T,[0,0,0,0,0,0,0], mask ,'alpha',iKineAlpha,'ilimit',1000)
            self.q = q;
            self.T = T;
        end
        
        function jointOne(self, joint)
            
            self.q(1,1) = joint;
        end
         function jointTwo(self, joint)
            
            self.q(1,2) = joint;
         end
         function jointThree(self, joint)
            
            self.q(1,3) = joint;
         end
         function jointFour(self, joint)
            
            self.q(1,4) = joint;
         end
         function jointFive(self, joint)
            
            self.q(1,5) = joint;
         end
         function jointSix(self, joint)
            
            self.q(1,6) = joint;
         end
         function jointSeven(self, joint)
            
            self.q(1,7) = joint;
         end
         
         function jointState = getJointState(self)
                    
             jointState = self.q;
         end
                
        function T = getCurrentTransforms(self)
            T = self.Fkine(self.q);
        end
        
        function qMatrix = calculateTraj(self, q1, q2)
    
            steps = 200;
            qMatrix = jtraj(q1,q2,steps);
        end
    end
    
end