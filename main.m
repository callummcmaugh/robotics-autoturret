% 41013 - Robotics Final Project
% By: Callum McMaugh & Ashley Baipas
% Description:
% Controlling Cyton robot to point at object in 3D space from
% stero camera through ROS.

% Packages: 
% RoboticsToolbox by Peter Corke.
% Matlab Image Toolbox.

% Hardware:
% Monoscopic Webcam
% Cyton robot - 7DOF

% % Simulation
cyton_s = Cyton()
cyton_s.PlotRobot3d();
cyton_s.playRobot3d([0,pi/2,0,-pi/2,0,pi/2,0])

% Robot
cyton_r = CytonROS();
cyton_r.Enable();
% pause(1);
cyton_r.MoveAllJoints([0,pi/2,0,-pi/2,0,pi/2,0])



% GUI
% Cyton_Gui(cyton_s);
% cyton_s.PlotRobot3d();
% cyton_s.playRobot3d([0,pi/2,0,-pi/2,0,pi/2,0])

axis equal
axis([-2,0.1,-1,1,-0.1,0.5]);

% Setup mimic
cyton_i = CytonInterface(cyton_s, cyton_r);

% Tracking
%  tt = TargetTracker(cyton_s);
 