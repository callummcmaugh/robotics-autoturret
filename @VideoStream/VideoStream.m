classdef VideoStream < handle
    properties
        cam_sub;
    end
    methods
        function obj = VideoStream()
            obj.Stream()
        end
        
        function Stream(obj)
            obj.cam_sub = rossubscriber('/camera/image_raw', @obj.CallbackStream);
            pause(1);
        end
        
        function PlotTF(obj)
           trplot(obj.t) 
        end
        
        function CallbackStream(obj, src, message)
%             message
            compressedFormatted = readImage(message);

            figure(2)
            imshow(compressedFormatted)
%             mtrans = message.Transforms.Transform.Translation;
%             mrot = message.Transforms.Transform.Rotation;
%             obj.trans = [ mtrans.X, mtrans.Y, mtrans.Z]
%             quat = [mrot.X, mrot.Y, mrot.Z, mrot.W];
%             obj.rot = quat2eul(quat);
%             obj.t = transl(obj.trans)*trotx(obj.rot(1))*troty(obj.rot(2))*trotz(obj.rot(3));
%             
%             
% %             Plot on CB?
%             obj.PlotTF();
        end
        
        function T = GetTranslation(obj)
            T = obj.trans; 
        end
        function R = GetRotation(obj)
            R = obj.trans;
        end
        function T = GetTransform(obj)
            T = obj.t;
        end
        function ClearCB(obj)
            obj.tf_sub = [];
        end
    end   
end